package ru.arubtsova.tm.api;

import ru.arubtsova.tm.model.Command;

public interface ICommandRepository {

    Command[] getTerminalCommands();

}
