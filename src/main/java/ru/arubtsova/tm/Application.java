package ru.arubtsova.tm;

import ru.arubtsova.tm.api.ICommandRepository;
import ru.arubtsova.tm.constant.ArgumentConst;
import ru.arubtsova.tm.constant.TerminalConst;
import ru.arubtsova.tm.model.Command;
import ru.arubtsova.tm.repository.CommandRepository;
import ru.arubtsova.tm.util.NumberUtil;

import java.util.Scanner;

public class Application {

    private static final ICommandRepository COMMAND_REPOSITORY = new CommandRepository();

    public static void main(String[] args) {
        System.out.println("*** WELCOME TO TASK-MANAGER ***");
        if (parseArgs(args)) System.exit(0);
        final Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.println("Enter command:");
            final String command = scanner.nextLine();
            parseCommand(command);
        }
    }

    private static void parseArg(final String arg) {
        if (arg == null) return;
        switch (arg) {
            case ArgumentConst.ARG_ABOUT:
                showAbout();
                break;
            case ArgumentConst.ARG_VERSION:
                showVersion();
                break;
            case ArgumentConst.ARG_HELP:
                showHelp();
                break;
            case ArgumentConst.ARG_INFO:
                showSystemInfo();
                break;
            default:
                showIncorrectArgument();
        }
    }

    private static void parseCommand(final String command) {
        if (command == null) return;
        switch (command) {
            case TerminalConst.CMD_ABOUT:
                showAbout();
                break;
            case TerminalConst.CMD_VERSION:
                showVersion();
                break;
            case TerminalConst.CMD_HELP:
                showHelp();
                break;
            case TerminalConst.CMD_INFO:
                showSystemInfo();
                break;
            case TerminalConst.CMD_COMMANDS:
                showCommands();
                break;
            case TerminalConst.CMD_ARGUMENTS:
                showArguments();
                break;
            case TerminalConst.CMD_EXIT:
                exit();
                break;
            default:
                showIncorrectCommand();
        }
    }

    public static void showIncorrectArgument() {
        System.out.println("Error! Argument not found");
    }

    public static void showIncorrectCommand() {
        System.out.println("Error! Command not found");
    }

    public static boolean parseArgs(String[] args) {
        if (args == null || args.length == 0) return false;
        String arg = args[0];
        parseArg(arg);
        return true;
    }

    public static void showAbout() {
        System.out.println("About:");
        System.out.println("Name: Anastasia Rubtsova");
        System.out.println("E-mail: Lafontana@mail.ru");
        System.out.println("Company: TSC");
    }

    public static void showVersion() {
        System.out.println("Version: 1.3.0");
    }

    public static void showSystemInfo() {
        final int availableProcessors = Runtime.getRuntime().availableProcessors();
        final long freeMemory = Runtime.getRuntime().freeMemory();
        final Long maxMemory = Runtime.getRuntime().maxMemory();
        final boolean isMaxMemory = maxMemory == Long.MAX_VALUE;
        final String maxMemoryValue = isMaxMemory ? "no limit" : NumberUtil.format(maxMemory);
        final long totalMemory = Runtime.getRuntime().totalMemory();
        final long usedMemory = totalMemory - freeMemory;
        System.out.println("System Information:");
        System.out.println("Available processors: " + availableProcessors);
        System.out.println("Free memory: " + NumberUtil.format(freeMemory));
        System.out.println("Maximum memory: " + maxMemoryValue);
        System.out.println("Total memory available to JVM: " + NumberUtil.format(totalMemory));
        System.out.println("Used memory by JVM: " + NumberUtil.format(usedMemory));
    }

    public static void showCommands() {
        final Command[] commands = COMMAND_REPOSITORY.getTerminalCommands();
        System.out.println("Available commands:");
        for (final Command command: commands) {
            final String name = command.getName();
            if (name == null) continue;
            System.out.println(name);
        }
    }

    public static void showArguments() {
        final Command[] commands = COMMAND_REPOSITORY.getTerminalCommands();
        System.out.println("Available arguments:");
        for (final Command command: commands) {
            final String arg = command.getArg();
            if (arg == null) continue;
            System.out.println(arg);
        }
    }

    public static void exit() {
        System.exit(0);
    }

    public static void showHelp() {
        System.out.println("Help:");
        final Command[] commands = COMMAND_REPOSITORY.getTerminalCommands();
        for (final Command command: commands) System.out.println(command);
    }

}
